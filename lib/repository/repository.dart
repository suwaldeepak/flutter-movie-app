import 'dart:async';

import 'package:flutter_assignment/models/movie_detail_model.dart';
import 'package:flutter_assignment/models/movie_model.dart';
import 'package:flutter_assignment/services/api_provider.dart';

class Repository {
  final apiProvider = ApiProvider();

  Future<List<Results>> fetchTrending(int pageKey, List<Results> list) =>
      apiProvider.fetchTrendingMovie(pageKey, list);

  Future<List<Results>> fetchPopularMovie(int pageKey, List<Results> list) =>
      apiProvider.fetchPopularMovie(pageKey, list);

  Future<List<Results>> fetchUpcomingMovie() =>
      apiProvider.fetchUpcomingMovie();

  Future<MovieDetailModel> fetchMovieDetail(int id) =>
      apiProvider.fetchMovieDetail(id);
}
