import 'package:flutter/material.dart';
import 'package:flutter_assignment/screens/movie_list.dart';

void main() {
  runApp(MaterialApp(
    home: MovieList(),
    theme: ThemeData(
      brightness: Brightness.light,
    ),
    darkTheme: ThemeData(
      brightness: Brightness.dark,
    ),
    themeMode: ThemeMode.dark,
    debugShowCheckedModeBanner: false,
  ));
}
