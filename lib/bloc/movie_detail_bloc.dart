import 'package:flutter_assignment/models/movie_detail_model.dart';
import 'package:flutter_assignment/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class MovieDetailBloc {
  final _repository = Repository();
  final _movieDetailFetcher = PublishSubject<MovieDetailModel>();

  Stream<MovieDetailModel> get allMovieDetail => _movieDetailFetcher.stream;

  fetchMovieDetail(int id) async {
    var result = await _repository.fetchMovieDetail(id);
    _movieDetailFetcher.sink.add(result);
  }

  dispose() {
    _movieDetailFetcher.close();
  }
}
