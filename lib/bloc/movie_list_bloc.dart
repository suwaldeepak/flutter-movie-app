import 'package:flutter_assignment/models/movie_model.dart';
import 'package:flutter_assignment/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class MovieListBloc {
  final _repository = Repository();
  final _trendingMovieFetcher = PublishSubject<List<Results>>();
  final _popularMovieFetcher = PublishSubject<List<Results>>();
  final _upComingMovieFetcher = PublishSubject<List<Results>>();

  Stream<List<Results>> get allTrendingMovie => _trendingMovieFetcher.stream;

  Stream<List<Results>> get allPopularMovie => _popularMovieFetcher.stream;

  Stream<List<Results>> get allUpcomingMovie => _upComingMovieFetcher.stream;

  fetchAllTrendingMovie(int page, List<Results> list) async {
    var result = await _repository.fetchTrending(page, list);
    _trendingMovieFetcher.sink.add(result);
  }

  fetchAllPopularMovie(int page, List<Results> list) async {
    var result = await _repository.fetchPopularMovie(page, list);
    _popularMovieFetcher.sink.add(result);
  }

  fetchAllUpComingMovie() async {
    var result = await _repository.fetchUpcomingMovie();
    _upComingMovieFetcher.sink.add(result);
  }

  dispose() {
    _trendingMovieFetcher.close();
    _popularMovieFetcher.close();
    _upComingMovieFetcher.close();
  }
}
