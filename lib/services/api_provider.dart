import 'dart:convert';
import 'package:flutter_assignment/constants/api_path.dart';
import 'package:flutter_assignment/constants/app_constants.dart';
import 'package:flutter_assignment/models/movie_detail_model.dart';
import 'package:flutter_assignment/models/movie_model.dart';
import 'package:http/http.dart' as http;

class ApiProvider {
  Future<List<Results>> fetchTrendingMovie(
      int page, List<Results> listModel) async {
    var query = "?api_key=" + apiKey + "&page=" + page.toString();
    var response = (await http.get(Uri.parse(baseUrl + trending + query)));
    if (response.statusCode == 200) {
      final data = MovieModel.fromJson(jsonDecode(response.body));
      listModel.addAll(data.results);

      return listModel;
    } else
      throw Exception('Failed to load data');
  }

  Future<List<Results>> fetchUpcomingMovie() async {
    List<Results> list = [];
    var query = "?api_key=" + apiKey;
    var response = (await http.get(Uri.parse(baseUrl + upcoming + query)));
    if (response.statusCode == 200) {
      final data = MovieModel.fromJson(jsonDecode(response.body));

      list.addAll(data.results);

      return list.take(10).toList();
    } else
      throw Exception('Failed to load data');
  }

  Future<List<Results>> fetchPopularMovie(
      int page, List<Results> listModel) async {
    var query = "?api_key=" + apiKey + "&page=" + page.toString();
    var response = (await http.get(Uri.parse(baseUrl + popular + query)));
    if (response.statusCode == 200) {
      final data = MovieModel.fromJson(jsonDecode(response.body));
      listModel.addAll(data.results);
      return listModel;
    } else
      throw Exception('Failed to load data');
  }

  Future<MovieDetailModel> fetchMovieDetail(int id) async {
    var query = id.toString() + "?api_key=" + apiKey;
    var response = (await http.get(Uri.parse(baseUrl + movieDetail + query)));
    if (response.statusCode == 200) {
      final data = MovieDetailModel.fromJson(jsonDecode(response.body));
      print(data.title);
      return data;
    } else
      throw Exception('Failed to load data');
  }
}
