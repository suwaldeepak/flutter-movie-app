class MovieModel {
  int page;
  List<Results> results;
  int totalPages;
  int totalResults;

  MovieModel({
      this.page, 
      this.results, 
      this.totalPages, 
      this.totalResults});

  MovieModel.fromJson(dynamic json) {
    page = json["page"];
    if (json["results"] != null) {
      results = [];
      json["results"].forEach((v) {
        results.add(Results.fromJson(v));
      });
    }
    totalPages = json["total_pages"];
    totalResults = json["total_results"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["page"] = page;
    if (results != null) {
      map["results"] = results.map((v) => v.toJson()).toList();
    }
    map["total_pages"] = totalPages;
    map["total_results"] = totalResults;
    return map;
  }

}

class Results {
  int id;
  bool adult;
  String backdropPath;
  List<int> genreIds;
  int voteCount;
  String originalLanguage;
  String originalTitle;
  String posterPath;
  String title;
  bool video;
  dynamic voteAverage;
  String releaseDate;
  String overview;
  double popularity;
  String mediaType;

  Results({
      this.id, 
      this.adult, 
      this.backdropPath, 
      this.genreIds, 
      this.voteCount, 
      this.originalLanguage, 
      this.originalTitle, 
      this.posterPath, 
      this.title, 
      this.video, 
      this.voteAverage, 
      this.releaseDate, 
      this.overview, 
      this.popularity, 
      this.mediaType});

  Results.fromJson(dynamic json) {
    id = json["id"];
    adult = json["adult"];
    backdropPath = json["backdrop_path"];
    genreIds = json["genre_ids"] != null ? json["genre_ids"].cast<int>() : [];
    voteCount = json["vote_count"];
    originalLanguage = json["original_language"];
    originalTitle = json["original_title"];
    posterPath = json["poster_path"];
    title = json["title"];
    video = json["video"];
    voteAverage = json["vote_average"];
    releaseDate = json["release_date"];
    overview = json["overview"];
    popularity = json["popularity"];
    mediaType = json["media_type"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["adult"] = adult;
    map["backdrop_path"] = backdropPath;
    map["genre_ids"] = genreIds;
    map["vote_count"] = voteCount;
    map["original_language"] = originalLanguage;
    map["original_title"] = originalTitle;
    map["poster_path"] = posterPath;
    map["title"] = title;
    map["video"] = video;
    map["vote_average"] = voteAverage;
    map["release_date"] = releaseDate;
    map["overview"] = overview;
    map["popularity"] = popularity;
    map["media_type"] = mediaType;
    return map;
  }

}