import 'package:flutter/material.dart';
import 'package:flutter_assignment/bloc/movie_detail_bloc.dart';
import 'package:flutter_assignment/constants/app_constants.dart';
import 'package:flutter_assignment/models/movie_detail_model.dart';

class MovieDetail extends StatefulWidget {
  final int movieId;

  const MovieDetail({Key key, this.movieId}) : super(key: key);

  @override
  _MovieDetailState createState() => _MovieDetailState();
}

class _MovieDetailState extends State<MovieDetail> {
  var bloc = MovieDetailBloc();

  @override
  void initState() {
    bloc.fetchMovieDetail(widget.movieId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Container(
        color: Color(blackColor),
        child: StreamBuilder<MovieDetailModel>(
          stream: bloc.allMovieDetail,
          builder: (context, snapshot) {
            if (snapshot.hasData)
              return Column(
                children: [
                  Image.network(imageBaseUrl + snapshot.data.backdropPath),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Column(
                      children: [
                        Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              snapshot.data.title,
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w500),
                            )),
                        Padding(
                          padding: EdgeInsets.only(top: 10),
                          child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                snapshot.data.overview,
                                style: TextStyle(
                                  fontSize: 14,
                                ),
                              )),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10),
                          child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Released date: " + snapshot.data.releaseDate,
                                style: TextStyle(
                                  fontSize: 14,
                                ),
                              )),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            else
              return Center(
                child: CircularProgressIndicator(),
              );
          },
        ),
      ),
    );
  }
}
