import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_assignment/bloc/movie_list_bloc.dart';
import 'package:flutter_assignment/constants/app_constants.dart';
import 'package:flutter_assignment/models/movie_model.dart';
import 'package:flutter_assignment/widget/movie_list_item.dart';

class MovieList extends StatefulWidget {
  const MovieList({Key key}) : super(key: key);

  @override
  _MovieListState createState() => _MovieListState();
}

class _MovieListState extends State<MovieList> {
  var bloc = MovieListBloc();
  List<Results> trendingList = [];
  List<Results> popularList = [];
  int _current = 0;

  final CarouselController _controller = CarouselController();

  ScrollController _scrollControllerTrending =
      ScrollController(initialScrollOffset: 0);
  ScrollController _scrollControllerPopular =
      ScrollController(initialScrollOffset: 0);
  var trendingPage = 1;
  var popularPage = 1;

  @override
  void initState() {
    bloc.fetchAllTrendingMovie(trendingPage, trendingList);
    bloc.fetchAllPopularMovie(popularPage, popularList);
    bloc.fetchAllUpComingMovie();
    _scrollControllerTrending.addListener(() {
      if (_scrollControllerTrending.offset >=
              _scrollControllerTrending.position.maxScrollExtent &&
          !_scrollControllerTrending.position.outOfRange) {
        trendingPage += 1;
        bloc.fetchAllTrendingMovie(trendingPage, trendingList);
      }
    });
    _scrollControllerPopular.addListener(() {
      if (_scrollControllerPopular.offset >=
              _scrollControllerPopular.position.maxScrollExtent &&
          !_scrollControllerPopular.position.outOfRange) {
        popularPage += 1;
        bloc.fetchAllPopularMovie(popularPage, popularList);
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
      ),
      body: Container(
        color: Color(blackColor),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: Container(
                height: MediaQuery.of(context).size.height / 2.5,
                child: StreamBuilder<List<Results>>(
                    stream: bloc.allUpcomingMovie,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Column(
                          children: [
                            CarouselSlider(
                              items: imageSliders(snapshot.data),
                              carouselController: _controller,
                              options: CarouselOptions(
                                  autoPlay: false,
                                  enlargeCenterPage: true,
                                  aspectRatio: 2.0,
                                  onPageChanged: (index, reason) {
                                    setState(() {
                                      _current = index;
                                    });
                                  }),
                            ),
                            Expanded(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children:
                                    snapshot.data.asMap().entries.map((entry) {
                                  return GestureDetector(
                                    onTap: () =>
                                        _controller.animateToPage(entry.key),
                                    child: Container(
                                      width: 5,
                                      margin: EdgeInsets.symmetric(
                                          vertical: 8.0, horizontal: 4.0),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white.withOpacity(
                                              _current == entry.key
                                                  ? 0.9
                                                  : 0.4)),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          ],
                        );
                      } else
                        return (Center(
                          child: CircularProgressIndicator(),
                        ));
                    }),
              ),
            ),
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.all(12),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Trending",
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.25,
                    child: StreamBuilder<List<Results>>(
                        stream: bloc.allTrendingMovie,
                        builder: (context, snapshot) {
                          if (snapshot.hasData)
                            return MovieListItem(
                              data: snapshot.data,
                              scrollController: _scrollControllerTrending,
                            );
                          else
                            return Text("");
                        }),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.all(12),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Popular",
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.25,
                    child: StreamBuilder<List<Results>>(
                        stream: bloc.allPopularMovie,
                        builder: (context, snapshot) {
                          if (snapshot.hasData)
                            return MovieListItem(
                              data: snapshot.data,
                              scrollController: _scrollControllerPopular,
                            );
                          else
                            return Text("");
                        }),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

List<Widget> imageSliders(List<Results> list) {
  return list
      .map((item) => Container(
            child: Container(
              margin: EdgeInsets.all(5.0),
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: Stack(
                    children: <Widget>[
                      Image.network(imageBaseUrl + item.backdropPath,
                          fit: BoxFit.cover, width: 1000.0),
                      Positioned(
                        bottom: 0.0,
                        left: 0.0,
                        right: 0.0,
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color.fromARGB(200, 0, 0, 0),
                                Color.fromARGB(0, 0, 0, 0)
                              ],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                            ),
                          ),
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 20.0),
                          child: Text(
                            item.title,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
          ))
      .toList();
}
