import 'package:flutter/material.dart';
import 'package:flutter_assignment/constants/app_constants.dart';
import 'package:flutter_assignment/models/movie_model.dart';
import 'package:flutter_assignment/screens/movie_detail.dart';

class MovieListItem extends StatelessWidget {
  final List<Results> data;
  final ScrollController scrollController;

  const MovieListItem({
    @required this.data,
    @required this.scrollController,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      controller: scrollController,
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => MovieDetail(
                          movieId: data[index].id,
                        )));
          },
          child: Container(
              child: Padding(
            padding: EdgeInsets.only(left: 12),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Image.network(
                  imageBaseUrl + data[index].posterPath,
                )),
          )),
        );
      },
    );
  }
}
