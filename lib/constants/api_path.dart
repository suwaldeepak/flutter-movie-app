const String baseUrl = "https://api.themoviedb.org/3/";
const String trending = "trending/all/day";
const String upcoming = "movie/upcoming";
const String popular = "movie/popular";
const String movieDetail = "movie/";
